# EasyTools
1.6.2.1

## 介绍
整合一些开发中常用的功能。以减少写测试代码的繁琐编写。

## 使用

### 自行打包
执行toexe.bat打包成exe工具。工具位于dist目录下。

### 成品文件
EXE下载链接：https://pan.baidu.com/s/1HT3te7ktIZMplB3Y_A2Etw?pwd=6666

## 代码环境
python3.9+

```
chardet==3.0.4
PyQt5==5.15.7
```

## 架构
```
EasyTools                     #
├─ config                       #
│   └─ config.py                  #
├─ logic                        #
│   ├─ dictview.py                # 字典可视化逻辑
│   ├─ my_random.py               # 随机字符串逻辑
│   └─ standard.py                # 代码编辑器逻辑
├─ ui                           #
│   ├─ dict_view.py               # 字典可视化UI
│   ├─ dir_show.py                # 目录树UI
│   ├─ random_char.py             # 随机字符组合UI
│   ├─ standard.py                # 代码编辑器UI
│   └─ zhengze.py                 # 正则UI
├─ util                         #
│   ├─ BaseConfig.py              #
│   ├─ basic.py                   #
│   └─ project_tree_maker.py      # 目录树生成类
├─ dialog.ui                    #
├─ main.py                      #
├─ README.md                    #
├─ toexe.bat                    #
├─ ui_base.py                   #
├─ ui_py.bat                    #
└─ z.ico                        #
```

## 功能介绍
### 正则辅助功能
基于python版正则测试。允许同时测试多行数据。
![image](doc/1%E6%AD%A3%E5%88%99.png)

### 字典展示功能
json数据制式展示，接受'或"的json数据
![image](doc/2%E5%AD%97%E5%85%B8.png)

### 制式编辑器【重制中】
制式代码编辑功能

### 随机字符串生成
个性化生成随机字符串
![image](doc/4%E9%9A%8F%E6%9C%BA.png)

### 目录树视图
生成目录树，方便文档视图的撰写
![image](doc/5%E7%9B%AE%E5%BD%95%E6%A0%91.png)


## 声明
1、本工具需要 __文件读写__ 权限用于读写配置文件。


2、本工具 __*不*__ 涉及 __网络请求__、__键鼠轨迹记录__，若检测到以上行为请立即清删此工具，并杀毒。


3、本工具为免费开源工具，仅用于辅助开发和学习交流使用，不得以任何形式用于收费用途。

## log
```
v1.6.4.1 2023年8月4日
【修复】修复临时配置读写异常问题。
【修复】修复random逻辑，排除字符导致崩溃问题。

v1.6.2.1 2022年11月23日
【修复】修复目录树在错误正则下崩溃的问题。
【优化】优化readme文档。

v1.6.2.0 2022年7月20日
【新增】允许部分页面缓存配置信息。
【新增】随机字符串允许设置为字符唯一。

v1.5.3.0 2022年7月19日
【新增】新增目录数输出工具。

v1.4.2.0 2021年11月13日
【新增】新增随机字符串功能。
```
