# -*- coding: utf-8 -*-
import random

# 数字
c_number = [chr(x) for x in range(ord('0'), ord('9')+1)]
# 小写
c_char_min = [chr(x) for x in range(ord('a'), ord('z')+1)]
# 大写
c_char_max = [chr(x) for x in range(ord('A'), ord('Z')+1)]
# 特殊字符
c_char_special = [chr(x) for x in range(ord('!'), ord('.')+1)]
c_char_special.remove('"')
c_char_special.remove("'")


def my_random(char_len=10, number=True, char_min=True, char_max=True, char_special=True,
              ex_char='', pass_char='', unique=False):
    """
    自定义输出
    :char_len     输出长度
    :number       数字
    :char_min     小写英文
    :char_max     大写英文
    :char_special 特殊字符
    :ex_char      自定义附加字符
    :pass_char    忽略字符
    :unique       不重复的
    """
    keywords = []
    res = ''
    if number:
        keywords += c_number
    if char_min:
        keywords += c_char_min
    if char_max:
        keywords += c_char_max
    if char_special:
        keywords += c_char_special
    if ex_char:
        if type(ex_char) == str:
            ex_char = list(ex_char.strip())
        keywords += ex_char
    if pass_char:
        if type(pass_char) == str:
            pass_char = list(pass_char.strip())
        for _k in pass_char:
            if _k in keywords:
                keywords.remove(_k)
    if not len(keywords):
        print('未配置输出字符类型')
        return ''
    if char_len <= 0:
        print('非法字符长度')
        return ''
    if not unique:
        for _ in range(char_len):
            res += random.choice(keywords)
    else:
        keywords = list(set(keywords))
        if char_len > len(keywords):
            print('唯一模式下，字符数不得超过总字符集')
            return ''
        for i in range(char_len):
            x = random.choice(keywords)
            keywords.remove(x)
            res += x
    return res
