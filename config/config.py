# -*- coding: utf-8 -*-
import os
from util.BaseConfig import BaseConfig4

config_mod = """[dict_view]

[dir_show]

[random_char]

[standard]

[zhengze]

"""


class Config(BaseConfig4):

    def __init__(self, filename) -> None:
        self.filename = filename
        if not os.path.isfile(filename):
            with open(filename, 'w', encoding='gbk') as f:
                f.write(config_mod)
                f.close()
        BaseConfig4.__init__(self, self.filename, 'gbk', no_section_create=True,\
             auto_save=True)

