# -*- coding: utf-8 -*-
import sys
import re
import os
if hasattr(sys, 'frozen'):
    os.environ['PATH'] = sys._MEIPASS + ";" + os.environ['PATH']
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtCore import QObject
from ui_base import Ui_Dialog
from ui.zhengze import ZhengzeUI
from ui.dict_view import DictViewUI
from ui.standard import StandardUI
from ui.random_char import RandomCharUI
from ui.dir_show import DirShowUI
from config.config import Config

class Dialog(ZhengzeUI, DictViewUI, StandardUI, RandomCharUI, DirShowUI, Ui_Dialog, QObject):
    
    def __init__(self):
        self._vars = {}
        self._base_var = '1.1.0.1'
        self.C = Config('config.ini')
        super().__init__()
        # QObject.__init__(self)
        # Ui_Dialog.__init__(self)
        # ZhengzeUI.__init__(self)
        # DictViewUI.__init__(self)
        # StandardUI.__init__(self)
        # RandomCharUI.__init__(self)
        # DirShowUI.__init__(self)
    
    def init(self):
        self.widget = QWidget()
        self.setupUi(self.widget)
        #正则卡片ui
        self.zhengze_init()
        self.dictview_init()
        self.standard_init()
        self.random_char_init()
        self.dir_show_init()
        
        a, b, c, d = re.findall('(\d+)\.(\d+)\.(\d+)\.(\d+)', self._base_var)[0]
        for _, value in self._vars.items():
            _a, _b, _c, _ = re.findall('(\d+)\.(\d+)\.(\d+)\.(\d+)', value)[0]
            b = int(b) + int(_a)
            c = int(c) + int(_b)
            d = int(d) + int(_c)
        title = '辅助工具 v{}.{}.{}.{}'.format(a, b, c, d)
        self.widget.setWindowTitle(title)
        self.widget.show()

    
if __name__=="__main__":
    app=QApplication(sys.argv) 
    ui=Dialog()  
    ui.init()
    sys.exit(app.exec_())  