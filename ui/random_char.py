# -*- coding: utf-8 -*-
from logic.my_random import my_random
from ui_base import Ui_Dialog

class RandomCharUI(Ui_Dialog):

    def __init__(self):
        super().__init__()
        self._vars['random_char'] = '1.1.0.0'
    
    def random_char_init(self):
        """初始化ui"""
        self.spinBox_random_res_len.setValue(self.C.get('random_char', '_len', 10))
        self.spinBox_random_res_sum.setValue(self.C.get('random_char', 'sum', 1))
        self.checkBox_random_use_0_9.setChecked(self.C.get('random_char', 'n', True))
        self.checkBox_random_use_a_z.setChecked(self.C.get('random_char', 'a', True))
        self.checkBox_random_use_A_Z.setChecked(self.C.get('random_char', 'A', True))
        self.checkBox_random_use_custom.setChecked(self.C.get('random_char', 's', False))
        self.checkBox_random_use_EX_key.setChecked(self.C.get('random_char', 'm', False))
        self.checkBox_random_unique.setChecked(self.C.get('random_char', 'unique', False))
        self.lineEdit_random_custom_keys.setText(self.C.get('random_char', 'my_char', ''))
        self.lineEdit_random_remove_keys.setText(self.C.get('random_char', 'rm_char', ''))
        self.pushButton_make_random_res.clicked.connect(self.pushButton_make_random_res_down)
        

    def pushButton_make_random_res_down(self):
        _len = self.spinBox_random_res_len.value()
        sum = self.spinBox_random_res_sum.value()
        n = self.checkBox_random_use_0_9.isChecked()
        a = self.checkBox_random_use_a_z.isChecked()
        A = self.checkBox_random_use_A_Z.isChecked()
        s = self.checkBox_random_use_EX_key.isChecked()
        m = self.checkBox_random_use_custom.isChecked()
        my_char = self.lineEdit_random_custom_keys.text()
        rm_char = self.lineEdit_random_remove_keys.text()
        unique = self.checkBox_random_unique.isChecked()
        #config
        self.C.set('random_char', '_len', _len)
        self.C.set('random_char', 'sum', sum)
        self.C.set('random_char', 'n', n)
        self.C.set('random_char', 'a', a)
        self.C.set('random_char', 'A', A)
        self.C.set('random_char', 's', s)
        self.C.set('random_char', 'm', m)
        self.C.set('random_char', 'my_char', my_char)
        self.C.set('random_char', 'rm_char', rm_char)
        self.C.set('random_char', 'unique', unique)
        self.C.save()
        ress = []
        for _ in range(sum):
            _res = my_random(_len, n, a, A, s, my_char if m else '', rm_char, unique)
            ress.append(_res)
        ress_text = '\n'.join(ress)
        self.textEdit_random_res_output.setPlainText(ress_text)
        return True